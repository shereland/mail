FROM node:8.11-alpine

WORKDIR /code
COPY package.json /code/package.json
RUN npm i

ENV TEMPLATES_DIR=/templates
COPY templates /templates

ENV DEFAULT_LANGUAGE=pt_br
ENV MICROSERVICE_PORT=4444

CMD [ "npm", "start" ]
