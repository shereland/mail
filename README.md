# Shereland mail

Microservice for sending e-mail by HTTP requests from other microservices.

It uses [pnp-mail-service](https://www.npmjs.com/package/pnp-email-service).

## Templates

### admin

```json
{
  "templateName": "admin",
  "templateOptions": {
    "title": "My title",
    "message": "Something important"
  },
  "emailOptions": {
    "to": "Admin <admin@example.com>"
  }
}
```

### contact

```json
{
  "templateName": "contact",
  "templateOptions": {
    "name": "George",
    "email": "george@orwell.com",
    "phone": "+55 11 9.8765-4321",
    "subject": "Doubts",
    "how_found_site": "Friends",
    "message": "My message"
  },
  "emailOptions": {
    "to": "Admin <admin@example.com>"
  }
}
```
